﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Jobson.Models
{
    public class CandidatoViewModel
    {
        [Required]
        [Display(Name = "CPF")]
        public string Cpf { get; set; }

        [Required]
        [Display(Name = "Usuário")]
        public string Usuario { get; set; }

        [Required]
        public string Nome { get; set; }

        [Required]
        [DataType(DataType.Password)]
        public string Senha { get; set; }

        [Required]
        [DataType(DataType.EmailAddress)]
        [Display(Name = "E-mail")]
        public string Email { get; set; }

        [Required]
        [Display(Name = "Endereço")]
        public string Endereco { get; set; }

        [Required]
        public string Bairro { get; set; }

        [Required]
        public string Cidade { get; set; }

        [Required]
        public string Estado { get; set; }

        [Required]
        [Display(Name = "Formação")]
        public string Formacao { get; set; }

        [Required]
        [Display(Name = "Conhecimento")]
        public string Conhecimento { get; set; }

        [Required]
        [Display(Name = "Publicação")]
        public string Publicacao { get; set; }

        [Required]
        [Display(Name = "Preferência")]
        public string Preferencia { get; set; }

        [Required]
        [Display(Name = "Idade")]
        public int Idade { get; set; }

        [Required]
        public Sexo Sexo { get; set; }

        [Required]
        public List<SelectListItem> ListaSexo { get; set; }
    }
}