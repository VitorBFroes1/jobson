﻿namespace Jobson.Models
{
    public enum Sexo
    {
        Masculino = 0,
        Feminino = 1
    }
}