﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Jobson.Models
{
    public class AnalistaViewModel
    {
        [Display(Name = "Nome")]
        [MaxLength(40)]
        [Required]
        public string Nome { get; set; }

        [Required]
        [Display(Name = "Usuário")]
        public string Usuario { get; set; }

        [Required]
        [Display(Name = "Senha")]
        [DataType(DataType.Password)]
        public string Senha { get; set; }

        [Required]
        [DataType(DataType.EmailAddress)]
        [Display(Name = "E-mail")]
        public string Email { get; set; }

        [Display(Name = "Telefone")]
        [MaxLength(11)]
        [Required]
        public string Telefone { get; set; }

        [Display(Name = "Endereço")]
        [Required]
        public string Endereco { get; set; }

        [Display(Name = "CNPJ da Empresa")]
        [Required]
        [MaxLength(13)]
        public string Cnpj { get; set; }

    }
}