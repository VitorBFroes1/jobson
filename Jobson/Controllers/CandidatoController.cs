﻿using Jobson.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.UI.WebControls;

namespace Jobson.Controllers
{
    public class CandidatoController : Controller
    {
        // GET: Candidato
        public ActionResult Index(string nome = "")
        {
            var candidato = new CandidatoViewModel();

            candidato.Nome = nome;

            return View(candidato);
        }

        // GET: Candidato/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: Candidato/Create
        public ActionResult Create()
        {
            var candidato = new CandidatoViewModel();

            candidato.ListaSexo = Mapear(new List<Sexo> { Sexo.Feminino, Sexo.Masculino });

            return View(candidato);
        }

        private List<SelectListItem> Mapear(List<Sexo> list)
        {
            var lista = new List<ListItem>();
            Array valores = Enum.GetValues(typeof(Sexo));

            foreach (var valor in valores)
            {
                lista.Add(new ListItem()
                {
                    Text = Enum.GetName(typeof(Sexo), valor),
                    Value = Convert.ToString((int)valor)
                });
            }
            return lista.Select(x => new SelectListItem() { Text = x.Text, Value = x.Value }).ToList();
        }

        // POST: Candidato/Create
        [HttpPost]
        public ActionResult Create(FormCollection collection)
        {
            try
            {
                // TODO: Add insert logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Candidato/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: Candidato/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Candidato/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: Candidato/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
