﻿using Jobson.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.UI.WebControls;

namespace Jobson.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Login()
        {
            return View();
        }
        public ActionResult Index()
        {

            return View();
        }
       
        public ActionResult Cadastro()
        {
            var contato = new CandidatoViewModel();

            contato.ListaSexo = Mapear(new List<Sexo>{ Sexo.Feminino, Sexo.Masculino});

            return View();
        }

        private List<SelectListItem> Mapear(List<Sexo> list)
        {
            var lista = new List<ListItem>();
            Array valores = Enum.GetValues(typeof(Sexo));

            foreach (var valor in valores)
            {
                lista.Add(new ListItem()
                {
                    Text = Enum.GetName(typeof(Sexo), valor),
                    Value = Convert.ToString((int)valor)
                });
            }
            return lista.Select(x => new SelectListItem() { Text = x.Text, Value = x.Value }).ToList();
        }

        [HttpPost]
        public ActionResult Cadastro(CandidatoViewModel contato)
        {

            contato.ListaSexo = Mapear(new List<Sexo> { Sexo.Feminino, Sexo.Masculino });

            return RedirectToActionPermanent("Index");
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}