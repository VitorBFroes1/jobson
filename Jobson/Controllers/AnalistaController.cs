﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Jobson.Controllers
{
    public class AnalistaController : Controller
    {
        // GET: Analista
        public ActionResult Index()
        {
            return View();
        }

        // GET: Analista/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: Analista/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Analista/Create
        [HttpPost]
        public ActionResult Create(FormCollection collection)
        {
            try
            {
                // TODO: Add insert logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Analista/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: Analista/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Analista/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: Analista/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
